module.exports = function(grunt) {
  "use strict";

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    clean: ["tmp"],

    concat: {
      storage: {
        files: {
          'storage.dist.js': [
            'bower_components/angular-cookie/angular-cookie.js',
            'tmp/storage-annotated.js',
          ]
        }
      },
    },

    ngAnnotate: {
      storage: {
        files: {
          'tmp/storage-annotated.js': ['storage.js']
        }
      }
    },

    karma: {
      unit: {
        configFile: 'karma.conf.js',
      }
    },
  });

  grunt.registerTask('build', ['clean', 'ngAnnotate', 'concat']);
  grunt.registerTask('test', ['karma']);
  grunt.registerTask('default', ['build', 'test']);
};
