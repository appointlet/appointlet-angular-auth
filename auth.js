(function() {
  "use strict";

  angular.module('appointlet.auth')

  /**
   * @ngdoc service
   * @name admin.service.Session
   * @module appointlet.admin
   *
   * @description
   * Session is a container for a user's access token, offering convenience
   * methods for creating and destroying it.
   */
  .service('Session', function($location, Storage) {
    /**
     * @ngdoc property
     * @name admin.service.Session#accessToken
     * @module appointlet.admin
     *
     * @description
     * OAuth2 access token assigned to the current user, if any.
     */
    this.accessToken = Storage.get('accessToken');

    /**
     * @ngdoc method
     * @name admin.service.Session#create
     * @description Creates a new session, persisting an access token to storage.
     * @param  {string} accessToken OAuth2 access token to assign to the client.
     */
    this.create = function(accessToken) {
      this.accessToken = accessToken;
      Storage.set('accessToken', accessToken);
    };

    /**
     * @ngdoc method
     * @name admin.service.Session#destroy
     * @description
     * Ends the current session, removing the access token from storage.
     */
    this.destroy = function() {
      this.accessToken = null;
      Storage.set('accessToken', null);
    };
  })

  /**
   * @ngdoc factory
   * @name auth.factory.Auth
   * @module appointlet.auth
   *
   * @description
   *
   * Exposes an API for authenticating users and logging them out.  Configures
   * HTTP and broadcasts events related to the state of auth.
   */
  .factory('Auth', function(Restangular, $http, Session, authService, QueryString, settings) {
    /**
     * @ngdoc method
     * @name auth.factory.Auth#authenticate
     * @description Stores the accessToken and applies it to HTTP requests.
     * @param  {string} accessToken to use to authenticate the client.
     */
    var authenticate = function(accessToken) {
      // this will store the access token for later sessions
      Session.create(accessToken);

      // apply the access token to future requests
      Restangular.setDefaultHeaders({
        Authorization: 'Bearer ' + accessToken
      });

      // tell the auth service to move forward with the queued
      // requests, applying the new access token to them.
      authService.loginConfirmed(null, function(httpConfig) {
        httpConfig.headers.Authorization = 'Bearer ' + accessToken;
        return httpConfig;
      });
    };

    /**
     * @ngdoc method
     * @name auth.factory.Auth#login
     * @description
     * Requests an accessToken from the server for the given username/password.
     * @param  {string} username to authenticate with.
     * @param  {string} password to authenticate with.
     */
    var login = function(username, password) {
      var data = {
        username: username,
        password: password,
        client_id: settings.CLIENT_ID,
        grant_type: 'password',
        scope: 'full',
      };

      return $http({
        method: 'POST',
        data: QueryString.encode(data),
        url: settings.WWW_URL + '/oauth2/token/',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },

        // if the server doesn't like the username/password it's going to
        // respond with a 401, which is going to make our http-auth-interceptor
        // believe that we need to login.  this disables it.
        ignoreAuthModule: true,

      }).then(function(resp) {
        // extract the access token from the response and use it to authenticate
        authenticate(resp.data.access_token);
      });
    };

    /**
     * @ngdoc method
     * @name auth.factory.Auth#logout
     * @description
     * Ends the client's current session and destroys the access token.
     */
    var logout = function() {
      // this will be sent to the server to destroy the access token
      var revocationData = {
        token: Session.accessToken,
      };

      // destroy the token locally
      Session.destroy();
      Restangular.defaultHeaders = null;

      // destroy the token on the server.
      return $http({
        method: 'POST',
        url: settings.WWW_URL + '/oauth2/revoke_token/',
        data: QueryString.encode(revocationData),
        ignoreAuthModule: true,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      });
    };

    return {
      login: login,
      logout: logout,
      authenticate: authenticate,
    };
  })

  /**
   * @ngdoc constant
   * @name auth.constant.AuthEvents
   * @module appointlet.auth
   *
   * @description Event names that can be broadcast from the Auth service.
   */
  .constant('AuthEvents', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
  });

})();
